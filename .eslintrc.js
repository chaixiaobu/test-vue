module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', '@vue/standard'],
  rules: {},
  globals: {
    AMap: false,
    AMapUI: false
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
