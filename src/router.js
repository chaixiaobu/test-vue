import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import test02 from './views/test/test02.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('./views/test/test.vue')
    },
    {
      path: '/test01',
      name: 'test01',
      component: () => import('./views/test/test01.vue')
    },
    {
      path: '/test02',
      name: 'test02',
      // 解决vue-amap中v.w.uh is not a constructor报错
      component: test02
    },
    {
      path: '/home',
      name: 'home',
      // 解决vue-amap中v.w.uh is not a constructor报错
      component: () => import('./views/home/home.vue')
    }
  ]
})
