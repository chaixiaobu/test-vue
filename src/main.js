import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
// 打印插件
import Print from 'vue-print-nb'
// 高德地图
import VueAMap from 'vue-amap'
// ElementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(Print)
Vue.use(VueAMap)

// 初始化高德地图
VueAMap.initAMapApiLoader({
  key: 'bb49e0d624be8a5a66fdc077fb55c237',
  plugin: [
    'AMap.Autocomplete',
    'AMap.PlaceSearch',
    'AMap.Scale',
    'AMap.OverView',
    'AMap.ToolBar',
    'AMap.MapType',
    'AMap.PolyEditor',
    'AMap.CircleEditor',
    'AMap.Geolocation',
    'AMap.Geocoder'
  ],
  uiVersion: '1.0.11',
  // 默认高德 sdk 版本为 1.4.4
  v: '1.4.4'
})

Vue.prototype.axios = axios

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
